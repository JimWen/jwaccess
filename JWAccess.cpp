/********************************************************************
	创建日期:	2014/01/08
	包含文件:	JWAccess.cpp JWAccess.h
	  创建者:	Jim Wen
        版本:	V1.0
	
		功能:	实现了一个简单的Access数据库访问类
				1.支持动态注册数据源,参见函数Connect,使用极其方便
				2.重新连接到新的数据源的时候会自动断开之前的连接,
				  你需要做的就是连接一个数据源开始操作,再连接到另
				  一个数据源开始操作
				3.提供了单例模式接口,不用每次使用前都构造一个对象
				  直接调用JWAccess::GetInstance()即可得到操作对象,
				  简单快捷
		说明:	这个类只是将使用Access数据库的过程简化了,并没有简
				化任何SQL操作,具体使用实例参见Demo,后续会继续完善
*********************************************************************/


#include "stdafx.h"
#include "JWAccess.h"

/******************引入动态注册DSN函数SQLConfigDataSource操作接口**********************/
#pragma comment( lib, "odbccp32.lib" )
#include <ODBCINST.H>

JWAccess::JWAccess()
{
	m_pRecordSet = NULL;
}

JWAccess::~JWAccess()
{
	DisConnect();
}

/**
 *功能:单例模式实现，返回JWAccess对象
 *参数:无
 *返回:JWAccess类型指针
 *其他:2013/11/20 By Jim Wen Ver1.0
**/
JWAccess* JWAccess::GetInstance()
{
	//单例模式
	static JWAccess instance;
	return &instance;
}

/**
 *功能:注册指定路径的Access数据库为数据源并打开数据库,绑定记录集和数据库
 *参数:szDbPath--以NULL结尾的包含指定Access数据库文件路径的字符串
 *返回:成功返回TRUE，失败返回FALSE
 *其他:2013/11/20 By Jim Wen Ver1.0
**/
BOOL JWAccess::Connect(LPCTSTR szDbPath )
{
	CString csTemp;
	csTemp.Format(TEXT("DSN=%s;DBQ=%s"), TEXT("my_db"), szDbPath);
	
	//动态注册数据源
	if(SQLConfigDataSource(NULL, ODBC_ADD_DSN, TEXT("Microsoft Access Driver (*.mdb)\0"), (LPCSTR)csTemp.GetBuffer(256)))
	{
		TRACE("/**************动态注册数据源成功！******************/\n");
	}
	else
	{
		TRACE("/**************创建DSN时，出现错误！******************/\n");
		return FALSE;
	}
	
	//打开数据库
	if (m_Database.IsOpen())
	{
		m_Database.Close();
	}
	BOOL bOpenFlag;
	ASSERT(TRUE == (bOpenFlag = m_Database.OpenEx(TEXT("DSN=my_db"),CDatabase::noOdbcDialog)));

	//将记录集和数据库绑定
	if (NULL != m_pRecordSet)
	{
		if (m_pRecordSet->IsOpen())
		{
			m_pRecordSet->Close();
		}
		
		delete(m_pRecordSet);
		m_pRecordSet = NULL;
	}
	if (NULL == m_pRecordSet)
	{
		m_pRecordSet = new CRecordset(&m_Database);
	}

	return bOpenFlag;
}

/**
 *功能:关闭Access数据库
 *参数:无
 *返回:无
 *其他:2013/11/20 By Jim Wen Ver1.0
**/
void JWAccess::DisConnect()
{
	if (NULL != m_pRecordSet)
	{
		if (m_pRecordSet->IsOpen())
		{
			m_pRecordSet->Close();
		}

		delete(m_pRecordSet);
		m_pRecordSet = NULL;
	}

	if (m_Database.IsOpen())
	{
		m_Database.Close();
	}
}

/**
 *功能:查询数据库
 *参数:szSQL--查询SELECT语句
 *返回:当前查询到的记录数
 *其他:2013/11/20 By Jim Wen Ver1.0
**/
long JWAccess::Select(LPCTSTR szSQL )
{
	if (m_pRecordSet->IsOpen())
	{
		m_pRecordSet->Close();
	}

	BOOL searchResult = m_pRecordSet->Open(CRecordset::snapshot, szSQL, CRecordset::none);

	if (TRUE == searchResult)
	{
		return m_pRecordSet->GetRecordCount();
	}
	else
	{
		return 0;
	}
}

/**
 *功能:执行数据库操作,包括UPDATE,INSERT,DELETE
 *参数:是否执行成功
 *返回:无
 *其他:2013/11/20 By Jim Wen Ver1.0
**/
BOOL JWAccess::Query(LPCTSTR szSQL )
{
	TRY
	{
		m_Database.ExecuteSQL( szSQL );
	}
	CATCH(CDBException, e)
	{
		return FALSE;
	}
	END_CATCH

	return TRUE;
}

/**
 *功能:获得指定字段编号对应的段值
 *参数:fieldIndex--字段索引号，从0开始
 *返回:字段值
 *其他:2013/11/20 By Jim Wen Ver1.0
 *注意:调用该函数时,索引号要强制转换成short类型,比如输入0如果不做转换
       可以转换成Short类型或char类型，会造成编译器歧义，必须指明
**/
CString JWAccess::GetField( const SHORT fieldIndex )
{
	CString csTemp;
	m_pRecordSet->GetFieldValue(fieldIndex, csTemp);
	return  csTemp;
}

/**
 *功能:获得指定字段名对应的段值
 *参数:fieldName--字段索引名称，以NULL结尾
 *返回:字段值
 *其他:2013/11/20 By Jim Wen Ver1.0
**/
CString JWAccess::GetField(LPCTSTR fieldName )
{
	CString csTemp;
	m_pRecordSet->GetFieldValue(fieldName, csTemp);
	return  csTemp;
}
